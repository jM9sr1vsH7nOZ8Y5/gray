#include "stdafx.h"
#include "viewplane.h"


void ViewPlane::setup(const vec3& cameraPosition, const vec3& lookAt, const vec3& cameraUp, const float_type& offset, const float_type& fov, const uvec2& imageSize)
{
	const auto viewDirection = glm::normalize(lookAt - cameraPosition);
	const auto U = glm::normalize(glm::cross(viewDirection, cameraUp));
	const auto V = -glm::normalize(glm::cross(U, viewDirection));

	const auto viewPlaneHalfWidth = offset * glm::tan<float_type>(fov / float_type(2));
	const auto viewPlaneHalfHeight = (viewPlaneHalfWidth * imageSize.y) / imageSize.x;

	m_CameraPosition = cameraPosition;
	m_TopLeftPoint = (cameraPosition + offset*viewDirection) - U*viewPlaneHalfWidth - V*viewPlaneHalfHeight;
	m_Size = vec2(viewPlaneHalfWidth, viewPlaneHalfHeight) * float_type(2);
	m_UIncrement = (U * m_Size.x) / float_type(imageSize.x);
	m_VIncrement = (V * m_Size.y) / float_type(imageSize.y);
}
