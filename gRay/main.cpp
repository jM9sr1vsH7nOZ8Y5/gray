// RayTracer.cpp : Defines the entry point for the console application.
//	 
#include "stdafx.h"
#include "timer.h"
#include "rendercontext.h"
#include <iostream>
#include <sstream>


int _tmain(int argc, _TCHAR* argv[])
{
	RenderContext renderContext;

	std::cout << "Describing scene ...";
	{
		const auto start = timer::now();

		// describe scene ...
		renderContext.setBackgroundColor(vec3(0.4, 0.4, 0.4));
		renderContext.setMaximumRecursionDepth(3);

		renderContext.setLookAt(vec3(0, 0, 0));
		renderContext.setUp(vec3(0, 1, 0));
		renderContext.setViewPlaneOffset(0.1);

		renderContext.setImageSize(uvec2(640, 480));
		renderContext.setFov(glm::radians<float_type>(90));

		renderContext.setMinimumDistance(glm::epsilon<float_type>()); // avoid self collisions
		renderContext.setMaximumDistance(std::numeric_limits<float_type>::max());
		renderContext.setSampling<HalfJitteredSampling3x3>();

		renderContext.setAmbientOcclusionSampleCount(512);


		// add lights
		{ 
			AreaLight::generateAreaLight(renderContext, vec3(40, 45, 45), vec3(-1, -1, 1), vec3(1), vec3(0.2f), uvec2(3), float_type(0.05));
			AreaLight::generateAreaLight(renderContext, vec3(-50, 45, 45), vec3(1, -1, 1), vec3(1), vec3(0.2f), uvec2(3), float_type(0.05));
		}

		// add objects ...
		{
			// checkerboard plane
			auto object = renderContext.addObject(new PlaneObject(0, vec3(0, 1, 0)));
			auto& material = object->getMaterial();
			material.setCheckerBoardPattern(4);
			material.setDiffuseColor(vec3(0.2, 0.2, 0.2));
			material.setShininess(5);
			material.setReflection(0.35);
		}
		{
			// red cube
			auto object = renderContext.addObject(new CuboidObject(vec3(-8, 2, 8), vec3(4)));
			auto& material = object->getMaterial();
			material.setDiffuseColor(vec3(1, 0, 0));
			material.setShininess(10);
			material.setReflection(0.2);
		}
		{
			// green sphere
			auto object = renderContext.addObject(new SphereObject(vec3(3, 2, 4), float_type(2)));
			auto& material = object->getMaterial();
			material.setDiffuseColor(vec3(0, 1, 0));
			material.setShininess(100);
			material.setReflection(0.3);
		}
		{
			// mirror sphere
			auto object = renderContext.addObject(new SphereObject(vec3(-3, 3, 0), float_type(3)));
			auto& material = object->getMaterial();
			material.setDiffuseColor(vec3(1, 1, 1));
			material.setShininess(200);
			material.setReflection(1);
		}
		{
			// bmw mesh
			const auto rotation = glm::rotate(mat4(), glm::radians<float_type>(-25.0), vec3(0, 1, 0));
			const auto scale = glm::scale(mat4(), vec3(0.0008));
			const auto translation = glm::translate(mat4(), vec3(1, 0, -6));
			auto object = renderContext.addObject(new MeshObject("meshes\\bmw\\BMW_M3_GTR.obj", "meshes\\bmw\\", translation * scale * rotation));
		}

		std::cout << " done. Elapsed time: " << timer::elapsed_millis(start) << " ms" << std::endl;
	}


	std::cout << "Rendering: image size = " << renderContext.getImageSize().x << "x" << renderContext.getImageSize().y << ", #objects " << renderContext.getObjects().size() << " ..." << std::endl;
	static const uint32_t steps(10);
	for (uint32_t step = 0; step < steps; ++step)
	{
		const auto phi = glm::radians(float_type(step * 360U / steps));
		const auto radius = float_type(10);
		renderContext.setEye(vec3(radius*glm::cos(phi), float_type(10), -radius*glm::sin(phi)));

		std::cout << "Rendering ... " << step * 100U / steps << "%";
		{
			const auto start = timer::now();

			// render ...
			const auto image = renderContext.render();

			std::cout << ". Step time: " << timer::elapsed_millis(start) << " ms" << std::endl;

			// save and show result
			std::stringstream sstr;
			sstr << "image_" << step << ".bmp";
			image.saveBmp(sstr.str().c_str());
		}
	}
	std::cout << "Rendering finished." << std::endl;

	return EXIT_SUCCESS;
}

