// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here

#pragma region - GLM -

#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\epsilon.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\random.hpp>

#include <glm\gtx\norm.hpp>


#pragma endregion


typedef glm::ivec2 ivec2;
typedef glm::uvec2 uvec2;

typedef float float_type;
typedef glm::detail::tvec2<float_type, glm::precision::defaultp> vec2;
typedef glm::detail::tvec3<float_type, glm::precision::defaultp> vec3;
typedef glm::detail::tvec4<float_type, glm::precision::defaultp> vec4;
typedef glm::detail::tmat3x3<float_type, glm::precision::defaultp> mat3;
typedef glm::detail::tmat4x4<float_type, glm::precision::defaultp> mat4;
typedef glm::detail::tquat<float_type, glm::precision::defaultp> quat;




#define __in__
#define __out__
#define __in_out__
