#include "stdafx.h"
#include "light.h"
#include "rendercontext.h"
#include "viewplane.h"


void AreaLight::generateAreaLight(RenderContext& renderContext, const vec3& position, const vec3& direction, const vec3& diffuseColor, const vec3& specularColor, const uvec2& size, const float_type& sampleDistance)
{
	const auto lightDiffuseColor = diffuseColor * (float_type(1) / float_type(size.x*size.y));
	const auto lightSpecularColor = specularColor * (float_type(1) / float_type(size.x*size.y));

	const auto offset = -vec2(size / glm::uint(2)) * sampleDistance;

	const auto minJitter = sampleDistance / float_type(4);
	const auto maxJitter = (float_type(3) * sampleDistance) / float_type(4);

	ViewPlane viewPlane;
	viewPlane.setup(position - glm::normalize(direction), position, vec3(0, 1, 0), float_type(1), glm::radians<float_type>(90), size);
	for (glm::uint y = 0; y < size.y; ++y)
	{
		for (glm::uint x = 0; x < size.x; ++x)
		{
			auto pointLight = renderContext.addLight(new PointLight());
			pointLight->setDiffuseColor(lightDiffuseColor);
			pointLight->setSpecularColor(lightSpecularColor);

			const auto pointLightOffset = offset + vec2(glm::linearRand(minJitter, maxJitter), glm::linearRand(minJitter, maxJitter));
			const auto p = float_type(y + pointLightOffset.y)*viewPlane.getVIncrement();
			const auto q = float_type(x + pointLightOffset.x)*viewPlane.getUIncrement();
			pointLight->setPosition(viewPlane.getTopLeftPoint() + p + q);
		}
	}
}
