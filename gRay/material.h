#pragma once
#ifndef __MATERIAL_H__
#define __MATERIAL_H__

class Material;

#include "image.h"


class Material final
{
public:
	inline Material()
		: m_CheckerBoardPattern(0),
		  m_Shininess(1),
		  m_Reflection(0),
		  m_Refraction(0),
		  m_Transmittance(0),
		  m_Texture(nullptr),
		  m_SpecularColor(1)
	{
	}

	const float_type& getShininess() const { return m_Shininess; }
	void setShininess(const float_type& value) { m_Shininess = value; }

	const float_type& getReflection() const { return m_Reflection; }
	void setReflection(const float_type& value) { m_Reflection = glm::clamp(value, float_type(0), float_type(1)); }

	const float_type& getRefraction() const { return m_Refraction; }
	void setRefraction(const float_type& value) { m_Refraction = value; }

	const vec3& getTransmittance() const { return m_Transmittance; }
	void setTransmittance(const vec3& value) { m_Transmittance = value; }

	const vec3& getDiffuseColor() const { return m_DiffuseColor; }
	void setDiffuseColor(const vec3& value) { m_DiffuseColor = value; }

	const vec3& getSpecularColor() const { return m_SpecularColor; }
	void setSpecularColor(const vec3& value) { m_SpecularColor = value; }

	const float_type& getCheckerBoardPattern() const { return m_CheckerBoardPattern; }
	void setCheckerBoardPattern(const float_type& value) { m_CheckerBoardPattern = value; }

	const Image* getTexture() const { return m_Texture; }
	void setTexture(const Image* value) { m_Texture = value; }

private:
	float_type m_Shininess;
	float_type m_Reflection;
	float_type m_Refraction;
	float_type m_CheckerBoardPattern;
	vec3 m_Transmittance;
	vec3 m_DiffuseColor;
	vec3 m_SpecularColor;
	const Image* m_Texture;
};


#endif // __MATERIAL_H__
