#include "stdafx.h"
#include "box.h"


#pragma region - AABB -

bool AABB::intersects(const Ray& ray, __out__ float_type& t) const
{
	const auto dirfrac = float_type(1) / ray.getDirection();
	const auto a = (m_Min - ray.getPosition()) * dirfrac;
	const auto b = (m_Max - ray.getPosition()) * dirfrac;

	const auto min = glm::min(a, b);
	const auto tmin = glm::max(glm::max(min.x, min.y), min.z);
	
	const auto max = glm::max(a, b);
	const auto tmax = glm::min(glm::min(max.x, max.y), max.z);

	if (tmax < 0 // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
		|| tmin > tmax) // if tmin > tmax, ray doesn't intersect AABB
	{
		t = tmax;
		return false;
	}

	t = tmin;
	return true;
}

#pragma endregion



#pragma region - AABBHierarchy -

AABBHierarchy::AABBHierarchy(const tinyobj::shape_t& shape)
{
	m_NumFaces = static_cast<uint32_t>(shape.mesh.indices.size() / 3);
	const auto NumAABBs = glm::max(static_cast<uint32_t>(glm::ceil(m_NumFaces / float_type(128))), 1U);
	m_NumFacesPerAABB = glm::max(static_cast<uint32_t>(glm::ceil(m_NumFaces / static_cast<float_type>(NumAABBs))), 1U);
	m_AABBs.reserve(NumAABBs);

	vec3 meshMin(std::numeric_limits<float_type>::max()), meshMax(std::numeric_limits<float_type>::min());
	const auto& vertices = shape.mesh.positions;
	const auto& indices = shape.mesh.indices;
	for (uint32_t aabbIndex = 0; aabbIndex < NumAABBs; ++aabbIndex)
	{
		vec3 min(std::numeric_limits<float_type>::max()), max(std::numeric_limits<float_type>::min());
		for (uint32_t index = getFaceIndexStart(aabbIndex); index < getFaceIndexEnd(aabbIndex); ++index)
		{
			const auto vIndex = getVec3<uint32_t, uint32_t>(indices, index);

			const auto v0 = getVec3<float_type, float>(vertices, vIndex.x);
			const auto v1 = getVec3<float_type, float>(vertices, vIndex.y);
			const auto v2 = getVec3<float_type, float>(vertices, vIndex.z);

			min = glm::min(min, glm::min(v0, glm::min(v1, v2)));
			max = glm::max(max, glm::max(v0, glm::max(v1, v2)));
		}
		m_AABBs.emplace_back(min, max);

		meshMin = glm::min(meshMin, min);
		meshMax = glm::max(meshMax, max);
	}

	m_AABB = AABB(meshMin, meshMax);
}

bool AABBHierarchy::traverse(__in_out__ uint32_t& index, __out__ const AABB** aabb)	const
{
	if (index < m_AABBs.size())
	{
		*aabb = &m_AABBs[index++];
		return true;
	}

	return false;
}

#pragma endregion
