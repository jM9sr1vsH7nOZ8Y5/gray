#pragma once
#ifndef __HIT_H__
#define __HIT_H__

class Hit;

#include "object.h"
#include "material.h"


class Hit final
{
public:
	inline Hit()
		: m_Object(nullptr),
		  m_Material(nullptr),
		  m_T(0)
	{
	}

	inline Hit(const Hit& other)
		: m_Object(other.m_Object),
	  	  m_Material(other.m_Material),
		  m_T(other.m_T),
		  m_Position(other.m_Position),
		  m_Normal(other.m_Normal),
		  m_TexCoord(other.m_TexCoord)
	{
	}

	inline void setT(const float_type& value) { m_T = value; }
	const float_type& getT() const { return m_T; }

	inline void setObject(const Object* object) { m_Object = object; }
	inline const Object* getObject() const { return m_Object; }

	inline void setMaterial(const Material* value) { m_Material = value; }
	inline const Material* getMaterial() const { return m_Material; }

	inline void setPosition(const vec3& value) { m_Position = value; }
	inline const vec3& getPosition() const { return m_Position; }

	inline void setNormal(const vec3& value) { m_Normal = value; }
	inline const vec3& getNormal() const { return m_Normal; }

	inline void setTexCoord(const vec2& value) { m_TexCoord = value; }
	inline const vec2& getTexCoord() const { return m_TexCoord; }

	static float_type computeT(const vec3& position, const Ray& ray);

private:
	const Object* m_Object;
	const Material* m_Material;
	float_type m_T;
	vec3 m_Position;
	vec3 m_Normal;
	vec2 m_TexCoord;
};


#endif // __HIT_H__
