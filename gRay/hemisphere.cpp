#include "stdafx.h"
#include "hemisphere.h"


Hemisphere::Hemisphere(const uint32_t& numSamples)
{
	m_Directions.resize(numSamples);
}

static inline vec3 randomize()
{
	const auto rand1 = glm::linearRand(float_type(0), float_type(1));
	const auto rand2 = glm::linearRand(float_type(0), float_type(1));

	const auto e = float_type(2) * glm::pi<float_type>() * rand2;
	const auto f = glm::sqrt(rand1);
	return vec3(glm::cos(e)*f, glm::sin(e)*f, float_type(1) - rand1);
}

void Hemisphere::setup()
{
	for (size_t n = 0; n < m_Directions.size(); ++n)
	{
		m_Directions[n] = randomize();
	}
}

inline vec3 sphericalToCartesian(const float_type& radius, const float_type& phi, const float_type& theta)
{
	return radius*vec3(glm::sin(phi)*glm::cos(theta),
					   glm::cos(phi),
					   -glm::sin(phi)*glm::sin(theta));
}

vec3 randomPosition(const float_type& radius)
{
	auto phi = (rand() % 2 == 0 ? float_type(-2) : float_type(2))*std::asin(std::sqrt(rand() / float_type(RAND_MAX))); // colatitute
	auto theta = glm::pi<float_type>()*(rand() / float_type(RAND_MAX)); // longitude, uniform on [0, Pi]

	return sphericalToCartesian(radius, phi, theta);
}

bool Hemisphere::generateRay(const Hit& hit, const uint32_t& numSamples, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const
{
	if (sampleId < numSamples)
	{
		vec3 d;
		do
		{
			d = randomPosition(1);
		} while (glm::dot(d, hit.getNormal()) <= float_type(0));

		weightedRay = WeightedRay(Ray(hit.getPosition(), d), float_type(1) / numSamples);

		++sampleId;
		return true;
	}

	return false;
}

bool Hemisphere::generateRay(const Hit& hit, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const
{
	if (sampleId < m_Directions.size())
	{
		const auto axis = glm::cross(hit.getNormal(), vec3(0, 1, 0));
		const auto angle = glm::acos(glm::dot(hit.getNormal(), vec3(0, 1, 0)));
		const auto q = glm::rotate(quat(), angle, axis);
		const auto d = q * m_Directions[sampleId];

		weightedRay = WeightedRay(Ray(hit.getPosition(), d), float_type(1) / float_type(m_Directions.size()));


		++sampleId;
		return true;
	}

	return false;
}
