#pragma once
#ifndef __OBJECT_H__
#define __OBJECT_H__

class Object;
class PlaneObject;
class CuboidObject;
class SphereObject;
class MeshObject;


#include "ray.h"
#include "hit.h"
#include "light.h"
#include "box.h"

#include "tiny_obj_loader.h"
#include "material.h"
#include "image.h"

#include <string>
#include <unordered_map>


class Object
{
protected:
	Object(const mat4& modelMatrix);

public:
	virtual ~Object();

	inline Material& getMaterial() { return m_Material; }
	inline const Material& getMaterial() const { return m_Material; }
	inline const mat4& getModelMatrix() const { return m_ModelMatrix; }
	inline const mat4& getInverseModelMatrix() const { return m_InverseModelMatrix; }
	inline const mat3& getNormalMatrix() const { return m_NormalMatrix; }
		
	virtual bool intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const = 0;
	virtual vec3 shade(const class RenderContext& renderContext, const Hit& hit) const;

protected:
	inline Ray rayToObjectSpace(const Ray& ray) const { return Ray(worldToObjectSpace(ray.getPosition()), (m_InverseModelMatrix * vec4(ray.getDirection(), 0)).xyz); }
	inline vec3 normalToWorldSpace(const vec3& value) const { return m_NormalMatrix * value; }
	inline vec3 objectToWorldSpace(const vec3& value) const { return (m_ModelMatrix * vec4(value, 1)).xyz; }
	inline vec3 worldToObjectSpace(const vec3& value) const { return (m_InverseModelMatrix * vec4(value, 1)).xyz; }

	float_type objectToWorldSpace(const Ray& ray, const Ray& rayInObjectSpace, const float_type& tInObjectSpace, __out__ vec3& hitInWorldSpace) const;

private:
	mat4 m_ModelMatrix;
	mat4 m_InverseModelMatrix;
	mat3 m_NormalMatrix;
	Material m_Material;
};




class PlaneObject final : public Object
{
private:
	typedef Object base;

public:
	PlaneObject(const float_type& offset, const vec3& normal);
	virtual ~PlaneObject();

	virtual bool intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const override;

	inline const float_type& getOffset() const { return m_Offset; }
	inline const vec3& getNormal() const { return m_Normal; }

private:
	float_type m_Offset;
	vec3 m_Normal;
};




class CuboidObject final : public Object
{
private:
	typedef Object base;

protected:
	vec3 getNormalAt(const vec3& position) const;

public:
	CuboidObject(const vec3& center, const vec3& size);
	virtual ~CuboidObject();

	virtual bool intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const override;

	inline const vec3& getCenter() const { return m_Center; }
	inline const vec3& getSize() const { return m_Size; }

private:
	vec3 m_Center;
	vec3 m_Size;
	AABB m_AABB;
};



class SphereObject final : public Object
{
private:
	typedef Object base;

public:
	SphereObject(const vec3& center, const float_type& radius);
	virtual ~SphereObject();

	virtual bool intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const override;

	inline const float_type& getRadius() const { return m_Radius; }
	inline const vec3& getCenter() const { return m_Center; }

private:
	float_type m_Radius;
	vec3 m_Center;
};




class MeshObject final : public Object
{
private:
	typedef Object base;

public:
	MeshObject(const std::string& filename, const std::string& materialDirectory, const mat4& modelMatrix);
	MeshObject(const std::string& filename, const std::string& materialDirectory);
	virtual ~MeshObject();

	virtual bool intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const override;

private:
	std::unordered_map<std::string, Image> m_Textures;
	std::vector<tinyobj::shape_t> m_Shapes;
	std::vector<AABBHierarchy> m_AABBHierarchies;
	std::vector<Material> m_Materials;
	AABB m_RootAABB;
};


#endif // __OBJECT_H__
