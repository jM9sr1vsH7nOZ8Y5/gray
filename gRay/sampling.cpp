#include "stdafx.h"
#include "sampling.h"



#pragma region - Sampling -

Sampling::Sampling(const ViewPlane& viewplane)
	: m_ViewPlane(viewplane)
{
}

Sampling::~Sampling()
{
}

vec3 Sampling::getViewPlanePoint(const uint32_t& u, const uint32_t& v) const
{
	return m_ViewPlane.getTopLeftPoint()
			+ (float_type(u) + float_type(0.5)) * m_ViewPlane.getUIncrement()
			+ (float_type(v) + float_type(0.5)) * m_ViewPlane.getVIncrement();
}

Ray Sampling::generateRay(const uint32_t& u, const uint32_t& v) const
{
	return generateRay(getViewPlanePoint(u, v));
}

Ray Sampling::generateRay(const vec3& viewPlanePoint) const
{
	return Ray(m_ViewPlane.getCameraPosition(), viewPlanePoint - m_ViewPlane.getCameraPosition());
}

#pragma endregion



#pragma region - UniformSampling -

UniformSampling::UniformSampling(const ViewPlane& viewPlane, const uvec2& size)
	: base(viewPlane),
	  m_Size(size)
{
}

UniformSampling::~UniformSampling()
{
}

void UniformSampling::setup()
{
	m_Offsets.clear();
	m_Weights.clear();
	m_Offsets.reserve(m_Size.x*m_Size.y);
	m_Weights.reserve(m_Size.x*m_Size.y);

	const auto size = vec2(getSize());

	const auto uIncrementLength = glm::length(getViewPlane().getUIncrement());
	const auto vIncrementLength = glm::length(getViewPlane().getVIncrement());

	const auto pixelSize = vec2(uIncrementLength, vIncrementLength);
	const auto subpixelSize = pixelSize / size;
	const auto adjustValue = -vec2(m_Size / glm::uint(2)) * subpixelSize;

	// compute offsets to the center of a pixel
	float_type weightSum(0);
	for (uint32_t y = 0; y < m_Size.y; ++y)
	{
		for (uint32_t x = 0; x < m_Size.x; ++x)
		{
			const auto xy = vec2(x, y);

			const auto weight = glm::exp(glm::length2(xy - glm::floor(size / float_type(2))) / float_type(-2));
			m_Weights.emplace_back(weight);
			weightSum += weight;

			const auto localOffset = xy * subpixelSize + adjustValue;
			const auto globalOffset = (xy - vec2(m_Size)) / size;
			m_Offsets.emplace_back((localOffset.x + globalOffset.x) * getViewPlane().getUIncrement()
								 + (localOffset.y + globalOffset.y) * getViewPlane().getVIncrement());
		}
	}

	for (auto& weight : m_Weights)
	{
		weight /= weightSum;
	}
}

bool UniformSampling::generateRay(const uint32_t& u, const uint32_t& v, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const
{
	if (sampleId < m_Offsets.size())
	{
		const auto offsetIndex = (sampleId / m_Size.x) + (sampleId % m_Size.x);
		weightedRay = WeightedRay(base::generateRay(base::getViewPlanePoint(u, v) + m_Offsets[offsetIndex]), m_Weights[offsetIndex]);
		++sampleId;
		return true;
	}

	return false;
}

#pragma endregion



#pragma region - RandomSampling -

RandomSampling::RandomSampling(const ViewPlane& viewPlane)
	: base(viewPlane)
{
}

RandomSampling::~RandomSampling()
{
}

void RandomSampling::setup()
{
}

bool RandomSampling::generateRay(const uint32_t& u, const uint32_t& v, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const
{
	if (sampleId == 0)
	{
		const auto uFac = glm::linearRand(float_type(-1), float_type(1)) / float_type(4);
		const auto vFac = glm::linearRand(float_type(-1), float_type(1)) / float_type(4);
		const auto viewPlanePoint = base::getViewPlanePoint(u, v) + uFac*getViewPlane().getUIncrement() + vFac*getViewPlane().getVIncrement();

		weightedRay = WeightedRay(base::generateRay(viewPlanePoint), float_type(1));
		++sampleId;
		return true;
	}

	return false;
}

#pragma endregion



#pragma region - HalfJitteredSampling -

HalfJitteredSampling::HalfJitteredSampling(const ViewPlane& viewPlane, const uvec2& size)
	: base(viewPlane, size)
{
}

HalfJitteredSampling::~HalfJitteredSampling()
{
}

void HalfJitteredSampling::setup()
{
	base::setup();

	const auto size = vec2(getSize());

	const auto uIncrementLength = glm::length(getViewPlane().getUIncrement());
	const auto vIncrementLength = glm::length(getViewPlane().getVIncrement());

	const auto subpixelSize = vec2(uIncrementLength, vIncrementLength) / size;
	const auto jitterValue = subpixelSize / float_type(4);
	
	// compute offsets to the center of a pixel
	float_type weightSum(0);
	for (uint32_t y = 0; y < getSize().y; ++y)
	{
		for (uint32_t x = 0; x < getSize().x; ++x)
		{
			const auto offsetIndex = y*getSize().x + x;
			const auto jitter = glm::linearRand(-jitterValue, jitterValue);

			const auto xy = vec2(x, y);

			const auto weight = glm::exp(glm::length2(xy - glm::floor(size / float_type(2)) + jitter) / float_type(-2));
			m_Weights[offsetIndex] = weight;
			weightSum += weight;

			m_Offsets[offsetIndex] += (jitter.x * getViewPlane().getUIncrement()) + (jitter.y * getViewPlane().getVIncrement());
		}
	}

	for (auto& weight : m_Weights)
	{
		weight /= weightSum;
	}
}

#pragma endregion
