#include "stdafx.h"
#include "timer.h"

#ifdef WIN32
	#define VC_EXTRALEAN
	#define WIN32_LEAN_AND_MEAN
		#include <windows.h>
	#undef WIN32_LEAN_AND_MEAN
	#undef VC_EXTRALEAN
#else
	#pragma message("timer is only supported in WIN32")
#endif

timer::value_t timer::frequency()
{
	static value_t frequency(0);
	if (frequency == value_t(0))
	{
	#ifdef WIN32
		::QueryPerformanceFrequency((LARGE_INTEGER*) &frequency);
	#endif
	}
	return frequency;
}

timer::value_t timer::now()
{
	value_t value;
	#ifdef WIN32
		::QueryPerformanceCounter((LARGE_INTEGER*) &value);
	#endif
	return value;
}

timer::value_t timer::seconds(const value_t& value)
{
	return value / frequency();
}

timer::value_t timer::millis(const value_t& value)
{
	return (1000i64 * value) / frequency();
}

timer::value_t timer::micros(const value_t& value)
{
	return (1000000i64 * value) / frequency();
}

timer::value_t timer::nanos(const value_t& value)
{
	return (1000000000i64 * value) / frequency();
}

timer::value_t timer::elapsed_seconds(const value_t& value)
{
	return seconds(now() - value);
}

timer::value_t timer::elapsed_millis(const value_t& value)
{
	return millis(now() - value);
}

timer::value_t timer::elapsed_micros(const value_t& value)
{
	return micros(now() - value);
}

timer::value_t timer::elapsed_nanos(const value_t& value)
{
	return nanos(now() - value);
}
