#pragma once
#ifndef __HEMISPHERE_H__
#define __HEMISPHERE_H__

#include "ray.h"
#include "hit.h"
#include <vector>


class Hemisphere final
{
public:
	Hemisphere(const uint32_t& numSamples);
	
	void setup();
	bool generateRay(const Hit& hit, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const;
	bool generateRay(const Hit& hit, const uint32_t& numSamples, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const;

private:
	std::vector<vec3> m_Directions;
};

#endif // __HEMISPHERE_H__
