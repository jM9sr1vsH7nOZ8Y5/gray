#include "stdafx.h"
#include "hit.h"


float_type Hit::computeT(const vec3& position, const Ray& ray)
{
	if (glm::abs(ray.getDirection().x > glm::epsilon<float_type>()))
	{
		return (position.x - ray.getPosition().x) / ray.getDirection().x;
	}
	
	if (glm::abs(ray.getDirection().y > glm::epsilon<float_type>()))
	{
		return (position.y - ray.getPosition().y) / ray.getDirection().y;
	}
	
	return(position.z - ray.getPosition().z) / ray.getDirection().z;
}
