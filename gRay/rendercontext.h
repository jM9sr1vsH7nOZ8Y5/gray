#pragma once
#ifndef __RENDERCONTEXT_H__
#define __RENDERCONTEXT_H__

class RenderContext;

#include "ray.h"
#include "object.h"
#include "light.h"
#include "image.h"
#include "sampling.h"
#include "viewplane.h"
#include "hemisphere.h"

#include <vector>




class RenderContext final
{
public:
	RenderContext();
	~RenderContext();

	inline const vec3& getEye() const { return m_Eye; }
	inline void setEye(const vec3& value) { m_Eye = value; }

	inline const vec3& getLookAt() const { return m_Center; }
	inline void setLookAt(const vec3& value) { m_Center = value; }

	inline const vec3& getUp() const { return m_Up; }
	inline void setUp(const vec3& value) { m_Up = value; }

	inline const uvec2& getImageSize() const { return m_ImageSize; }
	inline void setImageSize(const uvec2& value) { m_ImageSize = value; }

	inline const float_type& getFov() const { return m_Fov; }
	inline void setFov(const float_type& valueRad) { m_Fov = valueRad; }

	inline const vec3& getBackgroundColor() const { return m_BackgroundColor; }
	inline void setBackgroundColor(const vec3& value) { m_BackgroundColor = value; }
	
	inline const float_type& getMinimumDistance() const { return m_MinimumDistance; }
	inline void setMinimumDistance(const float_type& value) { m_MinimumDistance = value; }

	inline const float_type& getMaximumDistance() const { return m_MaximumDistance; }
	inline void setMaximumDistance(const float_type& value) { m_MaximumDistance = value; }

	inline const float_type& getViewPlaneOffset() const { return m_ViewPlaneOffset; }
	inline void setViewPlaneOffset(const float_type& value) { m_ViewPlaneOffset = value; }

	inline const uint16_t& getMaximumRecursionDepth() const { return m_MaximumRecursionDepth; }
	inline void setMaximumRecursionDepth(const uint16_t& value) { m_MaximumRecursionDepth = value; }

	template <typename T> inline T* addObject(T* object) { m_Objects.push_back(object); return object; }
	inline const std::vector<Object*>& getObjects() const { return m_Objects; }
	
	template <typename T> inline T* addLight(T* light) { m_Lights.push_back(light); return light; }
	inline const std::vector<Light*>& getLights() const { return m_Lights; }

	inline void setAmbientOcclusionSampleCount(const uint32_t& value) { m_AmbientOcclusionSampleCount = value; }
	inline const uint32_t& getAmbientOcclusionSampleCount() const { return m_AmbientOcclusionSampleCount; }

	inline Sampling* setSampling(Sampling* sampling)
	{
		if (m_Sampling != sampling)
		{
			delete m_Sampling;
			m_Sampling = sampling;
		}
		return m_Sampling;
	}

	template <typename T> inline T* setSampling() { return reinterpret_cast<T*>(setSampling(new T(m_ViewPlane))); }

	vec3 phongShading(const Hit& hit) const;
	bool traceRay(const Ray& ray, __out__ Hit& hit) const;
	bool traceRay(const Ray& ray, const uint16_t& depth, __out__ vec3& color) const;

	Image render();

	float_type ambientOcclusion(const Hit& hit) const;

private:
	void prepare();

	uvec2 m_ImageSize;

	vec3 m_Eye;
	vec3 m_Center;
	vec3 m_Up;

	vec3 m_UIncrement;
	vec3 m_VIncrement;
	vec3 m_ViewplaneBottomLeftPoint;

	float_type m_Fov;
	float_type m_MinimumDistance;
	float_type m_MaximumDistance;
	float_type m_ViewPlaneOffset;

	Sampling* m_Sampling;
	ViewPlane m_ViewPlane;

	Hemisphere m_Hemisphere;
	
	vec3 m_BackgroundColor;
	uint16_t m_MaximumRecursionDepth;
	std::vector<Object*> m_Objects;
	std::vector<Light*> m_Lights;

	uint32_t m_AmbientOcclusionSampleCount;
};


#endif // __RENDERCONTEXT_H__
