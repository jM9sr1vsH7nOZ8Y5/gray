#pragma once
#ifndef __BOX_H__
#define __BOX_H__

class AABB;
class AABBHierarchy;

#include "ray.h"
#include "tiny_obj_loader.h"


class AABB final
{
public:	  
	inline AABB()
	{
	}

	inline AABB(const vec3& min, const vec3& max)
		: m_Min(min), m_Max(max)
	{
	}

	bool intersects(const Ray& ray, __out__ float_type& t) const;

	inline const vec3& getMin() const { return m_Min; }
	inline const vec3& getMax() const { return m_Max; }

private:
	vec3 m_Min;
	vec3 m_Max;
};



class AABBHierarchy final
{
public:
	AABBHierarchy(const tinyobj::shape_t& shape);

	inline uint32_t getFaceIndexStart(const uint32_t& index) const { return index*m_NumFacesPerAABB; }
	inline uint32_t getFaceIndexEnd(const uint32_t& index) const { return glm::min((index + 1U)*m_NumFacesPerAABB, m_NumFaces); }

	inline const uint32_t& getNumFacesPerAABB() const { return m_NumFacesPerAABB; }
	inline const AABB& getRootAABB() const { return m_AABB; }

	bool traverse(__in_out__ uint32_t& index, __out__ const AABB** aabb) const;

	template <typename T, typename E> static inline glm::detail::tvec3<T, glm::precision::defaultp> getVec3(const std::vector<E>& vector, const uint32_t& index)
	{
		return glm::detail::tvec3<T, glm::precision::defaultp>(vector[3U*index + 0U], vector[3U*index + 1U], vector[3U*index + 2U]);
	}

	template <typename T, typename E> static inline glm::detail::tvec2<T, glm::precision::defaultp> getVec2(const std::vector<E>& vector, const uint32_t& index)
	{
		return glm::detail::tvec2<T, glm::precision::defaultp>(vector[2U*index + 0U], vector[2U*index + 1U]);
	}

private:
	AABB m_AABB;
	std::vector<AABB> m_AABBs;
	uint32_t m_NumFacesPerAABB;
	uint32_t m_NumFaces;
};

#endif // __BOX_H__ 
