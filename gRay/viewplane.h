#pragma once
#ifndef __VIEWPLANE_H__
#define __VIEWPLANE_H__


class ViewPlane final
{
public:
	void setup(const vec3& cameraPosition, const vec3& lookAt, const vec3& cameraUp, const float_type& offset, const float_type& fov, const uvec2& imageSize);

	inline const vec3& getCameraPosition() const { return m_CameraPosition; }
	inline const vec3& getCenter() const { return m_Center; }
	inline const vec3& getTopLeftPoint() const { return m_TopLeftPoint; }
	inline const vec3& getUIncrement() const { return m_UIncrement; }
	inline const vec3& getVIncrement() const { return m_VIncrement; }
	inline const vec2& getSize() const { return m_Size; }

private:
	vec3 m_CameraPosition;
	vec3 m_Center;
	vec3 m_TopLeftPoint;
	vec3 m_UIncrement;
	vec3 m_VIncrement;
	vec2 m_Size;
};


#endif // __VIEWPLANE_H__
