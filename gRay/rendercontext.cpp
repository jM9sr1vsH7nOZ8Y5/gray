#include "stdafx.h"
#include "rendercontext.h"
#include "hit.h"



RenderContext::RenderContext()
	: m_Sampling(nullptr),
	  m_AmbientOcclusionSampleCount(0),
	  m_Hemisphere(128)
{
	m_Hemisphere.setup();
}

RenderContext::~RenderContext()
{
	delete m_Sampling;

	for (auto& object : m_Objects)
	{
		delete object;
	}
	m_Objects.clear();

	for (auto& light : m_Lights)
	{
		delete light;
	}
	m_Lights.clear();
}

Image RenderContext::render()
{
	m_ViewPlane.setup(m_Eye, m_Center, m_Up, m_ViewPlaneOffset, m_Fov, m_ImageSize);
	
	Image image(m_ImageSize);
	if (m_Sampling)
	{
		m_Sampling->setup();

		#pragma omp parallel
		{
			#pragma omp for schedule(static) nowait
			for (int32_t v = 0; v < m_ImageSize.y; ++v)
			{
				for (uint32_t u = 0; u < m_ImageSize.x; ++u)
				{
					uint32_t sampleId(0);
					vec3 color(0, 0, 0);

					WeightedRay weightedRay;
					while (m_Sampling->generateRay(u, v, sampleId, weightedRay))
					{
						vec3 rayColor;
						if (!traceRay(weightedRay.getRay(), 0, rayColor))
						{
							rayColor = getBackgroundColor();
						}

						color += weightedRay.getWeight() * rayColor;
					}

					image.setRGB(u, v, color);
				}
			}
		}
	}

	return std::move(image);
}

bool RenderContext::traceRay(const Ray& ray, __out__ Hit& bestHit) const
{
	bestHit.setT(getMaximumDistance());
	bestHit.setObject(nullptr);

	Hit hit;
	for (const auto& object : m_Objects)
	{
		if (object->intersects(ray, getMinimumDistance(), bestHit, hit))
		{
			bestHit = hit;
		}
	}

	return bestHit.getObject() != nullptr;
}

bool RenderContext::traceRay(const Ray& ray, const uint16_t& depth, __out__ vec3& color) const
{
	Hit hit;
	if (depth <= getMaximumRecursionDepth() && traceRay(ray, hit))
	{
		const auto& objectToRender = hit.getObject();
		if (objectToRender)
		{
			color = objectToRender->shade(*this, hit);
			
			// reflection
			const auto& reflection = objectToRender->getMaterial().getReflection();
			if (reflection > glm::epsilon<float_type>())
			{
				const auto r = ray.getDirection() - (float_type(2) * glm::dot(ray.getDirection(), hit.getNormal()) / glm::length2(hit.getNormal())) * hit.getNormal();
				vec3 reflectionColor;
				if (!traceRay(Ray(hit.getPosition(), r), depth + 1, reflectionColor))
				{
					reflectionColor = getBackgroundColor();
				}

				color = (float_type(1) - reflection)*color + reflection*reflectionColor;
			}

			// TODO refraction
		}

		return true;
	}
	
	return false;
}

float_type RenderContext::ambientOcclusion(const Hit& hit) const
{
	float_type value(0);
	uint32_t sampleId(0);
	WeightedRay ambientOcclusionRay;
	while (m_Hemisphere.generateRay(hit, getAmbientOcclusionSampleCount(), sampleId, ambientOcclusionRay))
	{
		Hit ambientOcclusionHit;
		if (!traceRay(ambientOcclusionRay.getRay(), ambientOcclusionHit))
		{
			// ray hits hemisphere
			value += ambientOcclusionRay.getWeight();
		}
	}

	return value;
}

vec3 RenderContext::phongShading(const Hit& hit) const
{
	auto objectMaterial = hit.getMaterial();
	if (objectMaterial)
	{
		//return vec3(ambientOcclusion(hit));

		vec3 objectDiffuseColor;
		const auto& texture = objectMaterial->getTexture();
		if (texture)
		{
			auto pixelCoord = ivec2(glm::fract(hit.getTexCoord()) * vec2(texture->getSize()));
			pixelCoord.y = (texture->getSize().y - 1) - pixelCoord.y;
			objectDiffuseColor = texture->getRGB(pixelCoord);
		}
		else
		{
			objectDiffuseColor = objectMaterial->getDiffuseColor();
		}

		if (objectMaterial->getCheckerBoardPattern() > 0)
		{
			static const vec3 z(0, 0, 1);

			const auto angle = glm::acos(glm::clamp(glm::dot(hit.getNormal(), z) / (glm::length2(hit.getNormal()) * glm::length2(z)), float_type(-1), float_type(1)));
			const auto rotationAxis = glm::cross(hit.getNormal(), z);
			const auto p = (glm::length2(rotationAxis) > glm::epsilon<float_type>())
								? (glm::rotate(quat(), angle, rotationAxis) * hit.getPosition())
								: hit.getPosition();

			if ((static_cast<uint32_t>(glm::floor(p.x / objectMaterial->getCheckerBoardPattern())) % 2) == (static_cast<uint32_t>(glm::floor(p.y / objectMaterial->getCheckerBoardPattern())) % 2))
			{
				objectDiffuseColor = vec3(1) - objectDiffuseColor;
			}
		}

		vec3 color;
		for (const auto& light : m_Lights)
		{
			Hit shadowHit;
			Ray shadowRay(hit.getPosition(), light->getPosition() - hit.getPosition());
			if (!traceRay(shadowRay, shadowHit))
			{
				const auto l = glm::normalize(light->getPosition() - hit.getPosition());
				color += light->getDiffuseColor()*objectDiffuseColor*glm::max(glm::dot(l, hit.getNormal()), float_type(0));

				const auto v = glm::normalize(getEye() - hit.getPosition());
				const auto h = glm::normalize(l + v);
				
				const auto specularFactor = glm::pow(glm::max(glm::dot(hit.getNormal(), h), float_type(0)), objectMaterial->getShininess());
				color += light->getSpecularColor()*objectMaterial->getSpecularColor()*specularFactor;
			}
		}

		color = glm::clamp(color, float_type(0), float_type(1));

		if (getAmbientOcclusionSampleCount() > 0)
		{
			color *= ambientOcclusion(hit);
		}

		return color;
	}

	return getBackgroundColor();
}
