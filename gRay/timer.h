#pragma once
#ifndef __TIMER_H__
#define __TIMER_H__

#include <stdint.h>


class timer final
{
public:
	typedef int64_t value_t;

public:
	static value_t now();

	static value_t seconds(const value_t& value);
	static value_t millis(const value_t& value);
	static value_t micros(const value_t& value);
	static value_t nanos(const value_t& value);

	static value_t elapsed_seconds(const value_t& value);
	static value_t elapsed_millis(const value_t& value);
	static value_t elapsed_micros(const value_t& value);
	static value_t elapsed_nanos(const value_t& value);

private:
	static value_t frequency();
};

#endif
