#pragma once
#ifndef __SAMPLING_H__
#define __SAMPLING_H__

class Sampling;
class SimpleSampling;
class UniformSampling;
class RandomSampling;
class HalfJitteredSampling;

#include "ray.h"
#include "viewplane.h"
#include <vector>


class __declspec(novtable) Sampling
{
protected:
	Sampling(const ViewPlane& viewPlane);

	vec3 getViewPlanePoint(const uint32_t& u, const uint32_t& v) const;
	Ray generateRay(const uint32_t& u, const uint32_t& v) const;
	Ray generateRay(const vec3& viewPlanePoint) const;

public:
	virtual ~Sampling();

	virtual void setup() = 0;
	virtual bool generateRay(const uint32_t& u, const uint32_t& v, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const = 0;
	inline const ViewPlane& getViewPlane() const { return m_ViewPlane; }

private:
	const ViewPlane& m_ViewPlane;
};


#pragma region - RandomSampling -

class RandomSampling final : public Sampling
{
private:
	typedef Sampling base;

public:
	RandomSampling(const ViewPlane& viewPlane);
	virtual ~RandomSampling();

	virtual void setup() override;
	virtual bool generateRay(const uint32_t& u, const uint32_t& v, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const override;
};

#pragma endregion



#pragma region - UniformSampling -

class UniformSampling : public Sampling
{
private:
	typedef Sampling base;

protected:
	UniformSampling(const ViewPlane& viewPlane, const uvec2& size);

public:
	virtual ~UniformSampling();

	inline const uvec2& getSize() const { return m_Size; }

	virtual void setup() override;
	virtual bool generateRay(const uint32_t& u, const uint32_t& v, __in_out__ uint32_t& sampleId, __out__ WeightedRay& weightedRay) const override;

protected:
	std::vector<vec3> m_Offsets;
	std::vector<float_type> m_Weights;

private:
	uvec2 m_Size;
};

class UniformSampling1x1 final : public UniformSampling
{
public:
	inline UniformSampling1x1(const ViewPlane& viewPlane)
		: UniformSampling(viewPlane, uvec2(1, 1))
	{
	}
};

class UniformSampling3x3 final : public UniformSampling
{
public:
	inline UniformSampling3x3(const ViewPlane& viewPlane)
		: UniformSampling(viewPlane, uvec2(3, 3))
	{
	}
};

class UniformSampling5x5 final : public UniformSampling
{
public:
	inline UniformSampling5x5(const ViewPlane& viewPlane)
		: UniformSampling(viewPlane, uvec2(5, 5))
	{
	}
};

#pragma endregion



#pragma region - HalfJitteredSampling- 

class HalfJitteredSampling : public UniformSampling
{
private:
	typedef UniformSampling base;

protected:
	HalfJitteredSampling(const ViewPlane& viewPlane, const uvec2& size);

public:
	virtual ~HalfJitteredSampling();

	virtual void setup() override;
};

class HalfJitteredSampling3x3 final : public HalfJitteredSampling
{
public:
	inline HalfJitteredSampling3x3(const ViewPlane& viewPlane)
		: HalfJitteredSampling(viewPlane, uvec2(3, 3))
	{
	}
};

class HalfJitteredSampling5x5 final : public HalfJitteredSampling
{
public:
	inline HalfJitteredSampling5x5(const ViewPlane& viewPlane)
		: HalfJitteredSampling(viewPlane, uvec2(5, 5))
	{
	}
};

#pragma endregion



#endif // __SAMPLING_H__
