#include "stdafx.h"
#include "object.h"
#include "rendercontext.h"

#include <filesystem>



#pragma region - Object -

Object::Object(const mat4& modelMatrix)
	: m_ModelMatrix(modelMatrix),
	  m_InverseModelMatrix(glm::inverse(modelMatrix)),
	  m_NormalMatrix(glm::transpose(glm::inverse(modelMatrix)))
{
}

Object::~Object()
{
}

vec3 Object::shade(const RenderContext& renderContext, const Hit& hit) const
{
	return renderContext.phongShading(hit);
}

float_type Object::objectToWorldSpace(const Ray& ray, const Ray& rayInObjectSpace, const float_type& t, __out__ vec3& hitInWorldSpace) const
{
	hitInWorldSpace = objectToWorldSpace(rayInObjectSpace.trace(t));
	return Hit::computeT(hitInWorldSpace, ray);
}

#pragma endregion



#pragma region - PlaneObject -

PlaneObject::PlaneObject(const float_type& offset, const vec3& normal)
	: base(mat4()),
	  m_Offset(offset),
	  m_Normal(glm::normalize(normal))
{
}

PlaneObject::~PlaneObject()
{
}

bool PlaneObject::intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const
{
	const auto f = glm::dot(ray.getDirection(), m_Normal);
	if (glm::abs(f) > glm::epsilon<float_type>())
	{
		const auto t = -(glm::dot(ray.getPosition(), m_Normal) + m_Offset) / f;
		if (t >= minimumDistance && t < bestHit.getT())
		{
			hit.setT(t);
			if (glm::dot(t*ray.getDirection(), m_Normal) <= 0)
			{
				hit.setPosition(ray.trace(t));
				hit.setNormal(f >= float_type(0) ? -m_Normal : m_Normal);
				hit.setObject(this);
				hit.setMaterial(&getMaterial());
				return true;
			}
		}
	}

	return false;
}

#pragma endregion



#pragma region - CuboidObject -

CuboidObject::CuboidObject(const vec3& center, const vec3& size)
	: base(mat4()),
	  m_Center(center),
	  m_Size(size),
	  m_AABB(center - size / float_type(2), center + size / float_type(2))
{
}

CuboidObject::~CuboidObject()
{
}

vec3 CuboidObject::getNormalAt(const vec3& position) const
{
	vec3 normal;

	auto min = std::numeric_limits<float_type>::max();
	const auto localPoint = position - m_Center;
	auto distance = glm::abs(m_Size.x - glm::abs(localPoint.x));
	if (distance < min)
	{
		min = distance;
		normal = vec3(1 * glm::sign(localPoint.x), 0, 0);
	}

	distance = glm::abs(m_Size.y - glm::abs(localPoint.y));
	if (distance < min) 
	{
		min = distance;
		normal = vec3(0, 1 * glm::sign(localPoint.y), 0);
	}

	distance = glm::abs(m_Size.z - glm::abs(localPoint.z));
	if (distance < min)
	{
		min = distance;
		normal = vec3(0, 0, 1 * glm::sign(localPoint.z));
	}

	return normal;
}

bool CuboidObject::intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const
{
	float_type t;
	if (m_AABB.intersects(ray, t) && t >= minimumDistance && t < bestHit.getT())
	{
		hit.setT(t);
		hit.setPosition(ray.trace(t));
		hit.setNormal(getNormalAt(hit.getPosition()));
		hit.setObject(this);
		hit.setMaterial(&getMaterial());
		return true;
	}

	return false;
}

#pragma endregion



#pragma region - SphereObject -

SphereObject::SphereObject(const vec3& center, const float_type& radius)
	: base(glm::scale(glm::translate(mat4(), center), vec3(radius))),
	  m_Center(center), m_Radius(radius)
{
}

SphereObject::~SphereObject()
{
}

bool SphereObject::intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const
{
	const auto rayInObjectSpace = rayToObjectSpace(ray);

	const auto v = glm::dot(-rayInObjectSpace.getPosition(), rayInObjectSpace.getDirection());
	const auto d = float_type(1) - (glm::length2(rayInObjectSpace.getPosition()) - v*v);

	if (d < float_type(0))
	{
		return false;
	}

	const auto tInObjectSpace = v - glm::sqrt(d);
	vec3 hitInWorldSpace;
	const auto tInWorldSpace = objectToWorldSpace(ray, rayInObjectSpace, tInObjectSpace, hitInWorldSpace);
	if (tInWorldSpace >= minimumDistance && tInWorldSpace < bestHit.getT())
	{
		hit.setT(tInWorldSpace);
		hit.setPosition(hitInWorldSpace);
		hit.setNormal(glm::normalize(normalToWorldSpace(rayInObjectSpace.trace(tInObjectSpace))));
		hit.setObject(this);
		hit.setMaterial(&getMaterial());
		return true;
	}

	return false;
}

#pragma endregion



#pragma region - MeshObject -

MeshObject::MeshObject(const std::string& filename, const std::string& materialDirectory, const mat4& modelTransform)
	: base(modelTransform)
{
	const auto errString = tinyobj::LoadObj(m_Shapes, filename.c_str(), materialDirectory.c_str());
	if (errString.empty())
	{
		vec3 meshMin(std::numeric_limits<float_type>::max()), meshMax(std::numeric_limits<float_type>::min());
		for (const auto& shape : m_Shapes)
		{
			m_AABBHierarchies.emplace_back(shape);

			meshMin = glm::min(meshMin, m_AABBHierarchies.back().getRootAABB().getMin());
			meshMax = glm::max(meshMax, m_AABBHierarchies.back().getRootAABB().getMax());

			m_Materials.emplace_back(Material());
			auto& material = m_Materials.back();
			material.setDiffuseColor(vec3(shape.material.diffuse[0], shape.material.diffuse[1], shape.material.diffuse[2]));
			material.setSpecularColor(vec3(shape.material.specular[0], shape.material.specular[1], shape.material.specular[2]));
			material.setShininess(shape.material.shininess);
			material.setRefraction(shape.material.ior);
			material.setTransmittance(vec3(shape.material.transmittance[0], shape.material.transmittance[1], shape.material.transmittance[2]));

			if (!shape.material.diffuse_texname.empty())
			{
				if (m_Textures.find(shape.material.diffuse_texname) == m_Textures.end())
				{
					m_Textures[shape.material.diffuse_texname] = std::move(Image(materialDirectory + shape.material.diffuse_texname));
				}

				material.setTexture(&m_Textures[shape.material.diffuse_texname]);
			}
		}

		m_RootAABB = AABB(meshMin, meshMax);
	}
}

MeshObject::MeshObject(const std::string& filename, const std::string& materialDirectory)
	: MeshObject(filename, materialDirectory, mat4())
{
}

MeshObject::~MeshObject()
{
}

bool MeshObject::intersects(const Ray& ray, const float_type& minimumDistance, const Hit& bestHit, __out__ Hit& hit) const
{
	const auto rayInObjectSpace = rayToObjectSpace(ray);
	hit.setT(bestHit.getT());
	hit.setObject(nullptr);

	float_type aabbt;
	if (m_RootAABB.intersects(rayInObjectSpace, aabbt))
	{
		vec3 positionInWorldSpace;
		aabbt = objectToWorldSpace(ray, rayInObjectSpace, aabbt, positionInWorldSpace);
		if (aabbt >= minimumDistance && aabbt < hit.getT())
		{
			for (size_t shapeIndex = 0; shapeIndex < m_Shapes.size(); ++shapeIndex)
			{
				const auto& hierarchy = m_AABBHierarchies[shapeIndex];
				const auto& shape = m_Shapes[shapeIndex];
				const auto& vertices = shape.mesh.positions;
				const auto& indices = shape.mesh.indices;

				const AABB* aabb(nullptr);
				uint32_t hierarchyIndex(0), faceIndex(0);
				while (hierarchy.traverse(hierarchyIndex, &aabb))
				{
					if (aabb->intersects(rayInObjectSpace, aabbt))
					{
						aabbt = objectToWorldSpace(ray, rayInObjectSpace, aabbt, positionInWorldSpace);
						if (aabbt >= minimumDistance && aabbt < hit.getT())
						{
							for (uint32_t index = hierarchy.getFaceIndexStart(faceIndex); index < hierarchy.getFaceIndexEnd(faceIndex); ++index)
							{
								const auto vIndex = AABBHierarchy::getVec3<uint32_t, uint32_t>(indices, index);

								const auto v0 = AABBHierarchy::getVec3<float_type, float>(vertices, vIndex.x);
								const auto v1 = AABBHierarchy::getVec3<float_type, float>(vertices, vIndex.y);
								const auto v2 = AABBHierarchy::getVec3<float_type, float>(vertices, vIndex.z);

								const auto e1 = v1 - v0;
								const auto e2 = v2 - v0;

								const auto d_e2 = glm::cross(rayInObjectSpace.getDirection(), e2);

								auto f = glm::dot(d_e2, e1);
								if (glm::abs(f) > glm::epsilon<float_type>())
								{
									f = float_type(1) / f;
									const auto s = rayInObjectSpace.getPosition() - v0;
									const auto s_e1 = glm::cross(s, e1);
									
									const auto t = objectToWorldSpace(ray, rayInObjectSpace, glm::dot(s_e1, e2) * f, positionInWorldSpace);
									if (t >= minimumDistance && t < hit.getT())
									{
										const auto bx = glm::dot(d_e2, s) * f;
										const auto by = glm::dot(s_e1, rayInObjectSpace.getDirection()) * f;
										if (bx >= float_type(0) && by >= float_type(0) && (bx + by <= float_type(1)))
										{
											hit.setT(t);
											hit.setPosition(positionInWorldSpace);

											// interpolate normal
											const auto bz = float_type(1) - bx - by;
											const auto& normals = shape.mesh.normals;
											hit.setNormal(glm::normalize(normalToWorldSpace(bz*AABBHierarchy::getVec3<float_type, float>(normals, vIndex.x) 
																						  + bx*AABBHierarchy::getVec3<float_type, float>(normals, vIndex.y)
																						  + by*AABBHierarchy::getVec3<float_type, float>(normals, vIndex.z))));

											hit.setObject(this);
											hit.setMaterial(&m_Materials[shapeIndex]);

											const auto& texcoords = shape.mesh.texcoords;
											if (!texcoords.empty())
											{
												// interpolate texcoord
												hit.setTexCoord(bz*AABBHierarchy::getVec2<float_type, float>(texcoords, vIndex.x) 
															  + bx*AABBHierarchy::getVec2<float_type, float>(texcoords, vIndex.y)
															  + by*AABBHierarchy::getVec2<float_type, float>(texcoords, vIndex.z));
											}
										}
									}
								}
							}
						}
					}

					faceIndex = hierarchyIndex;
				}
			}
		}
	}

	return hit.getObject() != nullptr;
}

#pragma endregion
