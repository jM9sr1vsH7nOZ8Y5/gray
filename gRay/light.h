#pragma once
#ifndef __LIGHT_H__
#define __LIGHT_H__

#include <vector>


class Light
{
protected:
	inline Light()
	{
	}

public:
	const vec3& getDiffuseColor() const { return m_DiffuseColor; }
	void setDiffuseColor(const vec3& value) { m_DiffuseColor = value; }

	const vec3& getSpecularColor() const { return m_SpecularColor; }
	void setSpecularColor(const vec3& value) { m_SpecularColor = value; }

	const vec3& getPosition() const { return m_Position; }
	void setPosition(const vec3& value) { m_Position = value; }

private:
	vec3 m_Position;
	vec3 m_DiffuseColor;
	vec3 m_SpecularColor;
};



class PointLight final : public Light
{
public:
	inline PointLight()
	{
	}
};



class AreaLight
{
public:
	static void generateAreaLight(class RenderContext& renderContext, const vec3& position, const vec3& direction, const vec3& diffuseColor, const vec3& specularColor, const uvec2& size, const float_type& sampleDistance);
};



#endif // __LIGHT_H__
