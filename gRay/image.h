#pragma once
#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <vector>
#include <string>


class Image final
{
public:
	Image(const std::string& filename);
	Image(const ivec2& size);
	Image(Image&& other);
	Image();

	Image& operator = (Image&& other);

	void show() const;
	void saveBmp(const std::string& filename) const;

	inline void setRGB(const int& x, const int& y, vec3& rgb) { m_ImageData[y*m_Size.x + x] = rgb; }
	inline const vec3& getRGB(const int& x, const int&y) const { return m_ImageData[y*m_Size.x + x]; }
	inline const vec3& getRGB(const ivec2& xy) const { return m_ImageData[xy.y*m_Size.x + xy.x]; }
	inline const ivec2& getSize() const { return m_Size; }

private:
	std::vector<vec3> m_ImageData;
	ivec2 m_Size;
};


#endif // __IMAGE_H__
