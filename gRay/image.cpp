#include "stdafx.h"
#include "image.h"
#include <CImg.h>


Image::Image()
{
}

Image::Image(const std::string& filename)
{
	cimg_library::CImg<unsigned char> cimg(filename.c_str());

	m_Size = ivec2(cimg.width(), cimg.height());
	m_ImageData.resize(m_Size.x*m_Size.y);

	cimg_forY(cimg, y)
	{
		cimg_forX(cimg, x)
		{
			float_type rgb[3] = { 0, 0, 0 };
			cimg_forC(cimg, c)
			{
				rgb[c] = static_cast<float_type>(cimg(x, y, 0, c)) / float_type(255);
			}

			setRGB(x, y, vec3(rgb[0], rgb[1], rgb[2]));
		}
	}
}

Image::Image(const ivec2& size)
	: m_Size(size)
{
	m_ImageData.resize(size.x*size.y);
}

Image::Image(Image&& other)
{
	*this = other;
}

Image& Image::operator = (Image&& other)
{
	if (this != &other)
	{
		m_Size = other.m_Size;
		m_ImageData = std::move(other.m_ImageData);
	}

	return *this;
}

static cimg_library::CImg<unsigned char> toCImg(const Image& image)
{
	cimg_library::CImg<unsigned char> result(image.getSize().x, image.getSize().y, 1, 3);
	for (auto y = 0; y < image.getSize().y; ++y)
	{
		for (auto x = 0; x < image.getSize().x; ++x)
		{
			const auto color = image.getRGB(x, y) * float_type(255);
			result(x, y, 0, 0) = static_cast<unsigned char>(color.r);
			result(x, y, 0, 1) = static_cast<unsigned char>(color.g);
			result(x, y, 0, 2) = static_cast<unsigned char>(color.b);
		}
	}
	return std::move(result);
}

void Image::show() const
{
	const auto cimage = toCImg(*this);
	cimg_library::CImgDisplay display;
	display.display(cimage);
	while (!display.is_closed() && !display.is_closed())
	{
		display.wait();
	}
}

void Image::saveBmp(const std::string& filename) const
{
	toCImg(*this).save_bmp(filename.c_str());
}
