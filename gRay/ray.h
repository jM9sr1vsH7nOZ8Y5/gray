#pragma once
#ifndef __RAY_H__
#define __RAY_H__


class Ray final
{
public:
	inline Ray()
	{
	}

	inline Ray(const vec3& position, const vec3& direction)
		: m_Position(position), m_Direction(glm::normalize(direction))
	{
	}

	inline const vec3& getPosition() const { return m_Position; }
	inline const vec3& getDirection() const { return m_Direction; }

	inline vec3 trace(const float_type& t) const { return m_Position + t*m_Direction; }

private:
	vec3 m_Position;
	vec3 m_Direction;
};


class WeightedRay final
{
public:
	inline WeightedRay()
		: m_Weight(1)
	{
	}

	inline WeightedRay(const Ray& ray, const float_type& weight)
		: m_Ray(ray), m_Weight(weight)
	{
	}

	inline const Ray& getRay() const { return m_Ray; }
	inline const float_type& getWeight() const { return m_Weight; }

private:
	Ray m_Ray;
	float_type m_Weight;
};

#endif // __RAY_H__
